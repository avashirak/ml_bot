import asyncio

from aiogram import executor
from telegram_bot.telegrambot import main

if __name__ == '__main__':
    asyncio.run(main())
