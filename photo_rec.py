from hsemotion.facial_emotions import HSEmotionRecognizer
from PIL import Image
import numpy as np

# the model to use
model_name = 'enet_b0_8_best_afew'

# import and convert the file
file_name = 'photo.jpg'
im = Image.open(file_name)

# the process part
def Photo_Emotion_Recognition(file):
    a = np.asarray(file)
    fer = HSEmotionRecognizer(model_name=model_name, device='cpu')
    emotion, scores = fer.predict_emotions(a, logits=False)
    return emotion
