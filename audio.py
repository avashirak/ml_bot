import librosa
import numpy as np
from tensorflow import keras


# to extract features from audio
def extract_mfcc(filename):
    y, sr = librosa.load(filename, duration=3, offset=0.5)
    mfcc = np.mean(librosa.feature.mfcc(y=y, sr=sr, n_mfcc=40).T, axis=0)
    return mfcc


def voicerec(filename, model):
    # import audio file and prepare for prediction
    x = [extract_mfcc(filename)]
    x = np.array(x)
    x = np.expand_dims(x, -1)

    # save the result of prediction
    res = model.predict(x)

    emotions = ['Anger', 'Disgust', 'Fear', 'Happy', 'Neutral', 'Surprised', 'Sad']

    a = dict(zip(emotions, res[0]))
    result = list(a.keys())[list(a.values()).index(max(a.values()))]

    return result
