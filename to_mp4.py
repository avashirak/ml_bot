# Importing all necessary libraries
import cv2
import os
from video_rec import videorecognition


def videorec(path):
    # Read the video from specified path
    cam = cv2.VideoCapture(path)
    frame_count = int(cam.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = int(cam.get(cv2.CAP_PROP_FPS))

    try:

        # creating a folder named data
        if not os.path.exists('images'):
            os.makedirs('images')

    # if not created then raise error
    except OSError:
        print('Error: Creating directory of images')

    # frame
    currentframe = 0

    while cam.isOpened():

        # reading from frame
        ret, frame = cam.read()

        if ret:
            # if video is still left continue creating images
            name = './images/frame' + str(currentframe) + '.jpg'
            # print('Creating...' + name)

            # writing the extracted images
            cv2.imwrite(name, frame)

            # increasing counter so that it will
            # show how many frames are created
            currentframe += 30  # i.e. at 30 fps, this advances one second
            cam.set(cv2.CAP_PROP_POS_FRAMES, currentframe)

            if currentframe in range(350, 1000):
                break
        else:
            cam.release()
            break

    # Release all space and windows once done
    cam.release()
    cv2.destroyAllWindows()

    result = videorecognition(path='images')
    return result

