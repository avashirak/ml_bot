import time

from hsemotion.facial_emotions import HSEmotionRecognizer
from PIL import Image
import numpy as np
import os
import statistics
import os, shutil

'''
https://github.com/HSE-asavchenko/face-emotion-recognition/blob/main/python-package/hsemotion/facial_emotions.py
'''


def videorecognition(path):
    # the model to use
    model_name = 'enet_b0_8_best_afew'

    # Get the list of all files and directories
    path = "images"
    dir_list = os.listdir(path)

    # convert images into nparray list
    face_img_list = []
    for i in dir_list:
        file_name = 'images/' + i
        im = Image.open(file_name)
        a = np.asarray(im)
        face_img_list.append(a)

    # the process part
    fer = HSEmotionRecognizer(model_name=model_name, device='cpu')
    emotions, scores = fer.predict_multi_emotions(face_img_list, logits=False)

    for filename in os.listdir(path):
        file_path = os.path.join(path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    # show the emotion
    return statistics.mode(emotions)