import functools

from aiogram import types
from aiogram.dispatcher import FSMContext


def delete_previous_message_from_callback():
    def wrapper(func):
        @functools.wraps(func)
        async def wrapped(callback: types.CallbackQuery, state: FSMContext, *args, **kwargs):
            wrapped_function = await func(callback, state, *args, **kwargs)
            await callback.message.delete()
            return wrapped_function

        return wrapped

    return wrapper