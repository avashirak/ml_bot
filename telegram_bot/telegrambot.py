import asyncio
import logging
import os, sys
from pathlib import Path
from asyncio import sleep

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import CallbackQuery, Message, ContentTypes, ContentType
from PIL import Image
from tensorflow import keras
from scipy.io import wavfile
import noisereduce as nr

from photo_rec import Photo_Emotion_Recognition
import soundfile as sf
from telegram_bot.menu.menu import get_main_menu_keyboard
from telegram_bot.utils import delete_previous_message_from_callback
from to_mp4 import videorec
from audio import voicerec
from pydub import AudioSegment
from dotenv import load_dotenv


load_dotenv()

logging.basicConfig(level=logging.INFO)
storage = MemoryStorage()
bot = Bot(token=os.getenv('BOT_TOKEN'))
dp = Dispatcher(bot, storage=storage)
model = keras.models.load_model('lstm_model', compile=False)


def register_all_handlers(dp: Dispatcher):
    register_photo_receive(dp)


async def main():
    bot_commands = [
        types.BotCommand(command='/menu', description='Меню/Menu'),
        types.BotCommand(command='/start', description='Старт/Start'),
    ]
    await bot.set_my_commands(commands=bot_commands)
    register_all_handlers(dp)

    await dp.start_polling()


class BotStates(StatesGroup):
    waitchoice = State()
    photoreceive = State()
    photoanalysis = State()
    videoreceive = State()
    speechreceive = State()
    useranswer = State()


@dp.message_handler(commands=["start", "menu"], state='*')
async def start_handler(message: types.Message):
    print("Bot started")
    await BotStates.waitchoice.set()
    text = """Выберите метод"""
    keyboard = get_main_menu_keyboard()
    await message.answer(text, reply_markup=keyboard)


@delete_previous_message_from_callback()
async def photo_get(callback: CallbackQuery, state: FSMContext, *args, **kwargs):
    print("Bot started")
    await BotStates.photoreceive.set()
    text = """Загрузите фото"""
    await callback.message.answer(text)


@dp.message_handler(state=BotStates.photoreceive, content_types=['photo'])
async def photo_receive(message: types.Message, state: FSMContext):
    print("Bot State")
    async with state.proxy() as data:
        # try:
            photos = {}
            file_id = message.photo[-1].file_id
            await message.bot.download_file_by_id(file_id=file_id, destination=f'photos/{file_id}.jpg')
            analysis = Photo_Emotion_Recognition(Image.open(f'photos/{file_id}.jpg'))
            data['analysis'] = analysis
            print("Good")
            keyboard = get_main_menu_keyboard()
            await message.answer("Фото успешно загружено")
            await message.answer(analysis)
            await sleep(2)
            await message.answer('Выберите метод', reply_markup=keyboard)
            await BotStates.waitchoice.set()
        # except:
        #     print("Bad")
        #     await message.answer("Не удалось загрузить фото")


@delete_previous_message_from_callback()
async def voice_get(callback: CallbackQuery, state: FSMContext, *args, **kwargs):
    print("Bot started")
    await BotStates.speechreceive.set()
    text = """Запишите голосовое сообщение"""
    await callback.message.answer(text)


@dp.message_handler(state=BotStates.speechreceive, content_types=['voice'])
async def voice_receive(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        print("Voice receive")
        file_id = message.voice.file_id
        data['data'] = file_id
        await message.answer("Голосовое сообщение успешно загружено. Ожидайте.")
        await message.bot.download_file_by_id(file_id=file_id, destination=f'voices/{file_id}.ogg')
        voice = Path(f'voices/{file_id}.ogg')
        data, samplerate = sf.read(f'voices/{file_id}.ogg')
        sf.write(f'voices/{file_id}1.wav', data, samplerate)
        print(voice)

        stereo_audio = AudioSegment.from_file(f'voices/{file_id}1.wav', format="wav")
        mono_audios = stereo_audio.split_to_mono()
        mono_audios[0].export(f'voices/{file_id}2.wav', format="wav")

        rate, data = wavfile.read(f'voices/{file_id}2.wav')
        # perform noise reduction
        reduced_noise = nr.reduce_noise(y=data, sr=rate)
        wavfile.write(f"voices/{file_id}_reduced_noise.wav", rate, reduced_noise)

        analysis = voicerec(f"voices/{file_id}_reduced_noise.wav", model)

        print(analysis)
        await message.answer(analysis)
        await sleep(2)
        await message.answer('Какой ответ вы ожидали?\n1: Anger\n2: Disgust\n3: Fear\n4: Happy\n5: Neutral\n6: Surprised\n7: Sad')
        await BotStates.useranswer.set()


@dp.message_handler(state=BotStates.useranswer, content_types=['text'])
async def user_answer(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = message.text
        file_id = data['data']
        data, samplerate = sf.read(f'voices/{file_id}2.wav')
        sf.write(f'voices/{text}_{file_id}1.wav', data, samplerate)

        keyboard = get_main_menu_keyboard()
        await message.answer('Спасибо. Ваш ответ будет учтен.')
        await message.answer('Выберите метод', reply_markup=keyboard)
        await BotStates.waitchoice.set()


@delete_previous_message_from_callback()
async def video_get(callback: CallbackQuery, state: FSMContext, *args, **kwargs):
    print("Bot started")
    await BotStates.videoreceive.set()
    text = """Запишите видеосообщение"""
    await callback.message.answer(text)


@dp.message_handler(state=BotStates.videoreceive, content_types=['video_note'])
async def video_receive(message: types.Message, state: FSMContext):
    print("Video receive")
    video = message.video_note.file_id
    await message.answer("Видео успешно загружено")
    await message.bot.download_file_by_id(file_id=video, destination=f'videos/{video}.mp4')
    analysis = videorec(f'videos/{video}.mp4')
    keyboard = get_main_menu_keyboard()

    await message.answer(analysis)
    await sleep(2)
    await message.answer('Выберите метод', reply_markup=keyboard)
    await BotStates.waitchoice.set()



def register_photo_receive(dp: Dispatcher):
    # New proposal
    dp.register_callback_query_handler(
        photo_get,
        lambda callback_query: callback_query.data == 'Photo Recognize',
        state='*',
    )
    dp.register_message_handler(
        photo_receive,
        state=BotStates.photoreceive,
        content_types=ContentType.ANY,
    )

    dp.register_callback_query_handler(
        video_get,
        lambda callback_query: callback_query.data == 'Video Recognize',
        state='*',
    )
    dp.register_message_handler(
        video_receive,
        state=BotStates.photoreceive,
        content_types=ContentType.ANY,
    )

    dp.register_callback_query_handler(
        voice_get,
        lambda callback_query: callback_query.data == 'Voice Recognize',
        state='*',
    )
    dp.register_message_handler(
        voice_receive,
        state=BotStates.photoreceive,
        content_types=ContentType.ANY,
    )