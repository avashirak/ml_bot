from aiogram import types


def get_main_menu_keyboard(**kwargs) -> types.InlineKeyboardMarkup:
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    voice = types.InlineKeyboardButton(
        text="Voice Recognize",
        callback_data="Voice Recognize",
    )
    photo = types.InlineKeyboardButton(
        text="Photo Recognize",
        callback_data="Photo Recognize",
    )
    video = types.InlineKeyboardButton(
        text="Video Recognize",
        callback_data="Video Recognize",
    )

    keyboard.add(photo, video)
    keyboard.add(voice)

    return keyboard
